Para a configuração do nosso pipeline CI-CD, será necessário configurar algumas dependências em nossa console do **GCP.**

- Os recursos necessários para este tutorial :
    - Cloud Cloud GKE;
        - Para criarmos nossos pods Kubernetes
    - Google Compute Engine
        - Para criar nossa máquina Bastion → Ela faz o gerenciamento de nossa infraestrutura CI-CD

Primeiramente iremos criar nosso secrets diretamente utilizando 

`kubectl apply -f my-secret.yml`

Arquivo do Secret

```yaml
Utilizando Base64

apiVersion: v1
kind: Secret
metadata:
  name: my-secret
type: Opaque
data:
  ROOT_PASSWORD:U2VuaGExMjM=
  MYSQL_DATABASE: TWV1YmFuY28=

Informações sem codificação em Base64

apiVersion: v1
kind: Secret
metadata:
  name: my-secret
type: Opaque
data:
  ROOT_PASSWORD: Senha123
  MYSQL_DATABASE: Meubanco
```

Após isso crie o serviço do loadbalancer para obter as informações sobre o external IP 

 - Feito essa modificação copie o ip do loadbalancer e insira no arquivo presente em:
 app → js.js


Feito as configurações de Push no diretório 
